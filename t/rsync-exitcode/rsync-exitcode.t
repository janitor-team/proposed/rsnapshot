#!/usr/bin/perl

use strict;
use Test::More tests => 2;
use SysWrap;

mkdir("/home/sam/src/rsnapshot//t//support/files/a") unless -d "/home/sam/src/rsnapshot//t//support/files/a";
execute("cp /home/sam/src/rsnapshot//t/support/files/template/a/1 /home/sam/src/rsnapshot//t/support/files/template/a/2 /home/sam/src/rsnapshot//t//support/files/a/");

ok(0 != rsnapshot("-c /home/sam/src/rsnapshot//t//rsync-exitcode/conf/rsync-exitcode-bad.conf hourly"));
ok(0 == rsnapshot("-c /home/sam/src/rsnapshot//t//rsync-exitcode/conf/rsync-exitcode-good.conf hourly"));

execute("rm -f /home/sam/src/rsnapshot//t//support/files/a/1 /home/sam/src/rsnapshot//t/support/files/a/2");
